/*****************************************************************************/

/*
 *      syslog.c  -- Syslog "abstraction" routines.
 *
 *      Copyright (C) 1998  Thomas Sailer (sailer@ife.ee.ethz.ch)
 *
 *      This program is free software; you can redistribute it and/or modify
 *      it under the terms of the GNU General Public License as published by
 *      the Free Software Foundation; either version 2 of the License, or
 *      (at your option) any later version.
 *
 *      This program is distributed in the hope that it will be useful,
 *      but WITHOUT ANY WARRANTY; without even the implied warranty of
 *      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *      GNU General Public License for more details.
 *
 *      You should have received a copy of the GNU General Public License
 *      along with this program; if not, write to the Free Software
 *      Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 *  Please note that the GPL allows you to use the driver, NOT the radio.
 *  In order to use the radio, you need a license from the communications
 *  authority of your country.
 *
 */

/*****************************************************************************/

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#define _COMPILE_LIBFPGA
#include "libfpga.h"

/* ---------------------------------------------------------------------- */

#if defined(__MINGW32__) || defined(__CYGWIN32__) || defined(__CYGWIN__) || defined(GO32)

int syslogprintf(unsigned vl, const char *format, va_list ap)
{
	return 0;
}

int syslogopen(const char *ident)
{
	return -1;
}

#else

#include <syslog.h>

int syslogprintf(unsigned vl, const char *format, va_list ap)
{
        static const int logprio[] = { LOG_ERR, LOG_INFO };
	vsyslog((vl > 1) ? LOG_DEBUG : logprio[vl], format, ap);
	return 0;
}

int syslogopen(const char *ident)
{
	openlog(ident, LOG_PID, LOG_USER);
	return 0;
}

#endif

/* ---------------------------------------------------------------------- */
