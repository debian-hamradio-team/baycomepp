/*****************************************************************************/

/*
 *      epp_fpga.h  -- HDLC packet radio modem for EPP using FPGA utility.
 *
 *      Copyright (C) 1998  Thomas Sailer (sailer@ife.ee.ethz.ch)
 *
 *      This program is free software; you can redistribute it and/or modify
 *      it under the terms of the GNU General Public License as published by
 *      the Free Software Foundation; either version 2 of the License, or
 *      (at your option) any later version.
 *
 *      This program is distributed in the hope that it will be useful,
 *      but WITHOUT ANY WARRANTY; without even the implied warranty of
 *      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *      GNU General Public License for more details.
 *
 *      You should have received a copy of the GNU General Public License
 *      along with this program; if not, write to the Free Software
 *      Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 *  Please note that the GPL allows you to use the driver, NOT the radio.
 *  In order to use the radio, you need a license from the communications
 *  authority of your country.
 *
 */

/*****************************************************************************/
/* automatically generated, do not edit */

#define FPGA_PART "XCS10_PC84"
#define FPGA_BOUND 344
#define FPGA_FRAMELEN 166
#define FPGA_NUMFRAMES 572
/*#define FPGA_CONFIGSIZE ((FPGA_FRAMELEN*FPGA_NUMFRAMES+56)/8)*/
#define FPGA_CONFIGSIZE 11876
#define FPGA_BOUNDSIZE ((FPGA_BOUND+7)/8)

/* safe boundary */
static const unsigned char fpga_safebound[] = {
	0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 
	0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 
	0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0x3f, 
	0xff, 0xff, 0x7f, 0xff, 0xff, 0xff, 0xff, 0xff, 
	0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 
	0xff, 0xff, 0xff, 0xff
};

/* IO pin names */
#define FPGA_PIN_RAMD0_T              253
#define FPGA_PIN_RAMD0_O              254
#define FPGA_PIN_RAMD0_I              255
#define FPGA_PIN_RAMD1_T              241
#define FPGA_PIN_RAMD1_O              242
#define FPGA_PIN_RAMD1_I              243
#define FPGA_PIN_RAMD2_T              229
#define FPGA_PIN_RAMD2_O              230
#define FPGA_PIN_RAMD2_I              231
#define FPGA_PIN_RAMD3_T              217
#define FPGA_PIN_RAMD3_O              218
#define FPGA_PIN_RAMD3_I              219
#define FPGA_PIN_RAMD4_T              220
#define FPGA_PIN_RAMD4_O              221
#define FPGA_PIN_RAMD4_I              222
#define FPGA_PIN_RAMD5_T              232
#define FPGA_PIN_RAMD5_O              233
#define FPGA_PIN_RAMD5_I              234
#define FPGA_PIN_RAMD6_T              244
#define FPGA_PIN_RAMD6_O              245
#define FPGA_PIN_RAMD6_I              246
#define FPGA_PIN_RAMD7_T              256
#define FPGA_PIN_RAMD7_O              257
#define FPGA_PIN_RAMD7_I              258
#define FPGA_PIN_RAMA0_T              259
#define FPGA_PIN_RAMA0_O              260
#define FPGA_PIN_RAMA0_I              261
#define FPGA_PIN_RAMA1_T              262
#define FPGA_PIN_RAMA1_O              263
#define FPGA_PIN_RAMA1_I              264
#define FPGA_PIN_RAMA2_T              283
#define FPGA_PIN_RAMA2_O              284
#define FPGA_PIN_RAMA2_I              285
#define FPGA_PIN_RAMA3_T              295
#define FPGA_PIN_RAMA3_O              296
#define FPGA_PIN_RAMA3_I              297
#define FPGA_PIN_RAMA4_T              301
#define FPGA_PIN_RAMA4_O              302
#define FPGA_PIN_RAMA4_I              303
#define FPGA_PIN_RAMA5_T              313
#define FPGA_PIN_RAMA5_O              314
#define FPGA_PIN_RAMA5_I              315
#define FPGA_PIN_RAMA6_T              325
#define FPGA_PIN_RAMA6_O              326
#define FPGA_PIN_RAMA6_I              327
#define FPGA_PIN_RAMA7_T              337
#define FPGA_PIN_RAMA7_O              338
#define FPGA_PIN_RAMA7_I              339
#define FPGA_PIN_RAMA8_T              328
#define FPGA_PIN_RAMA8_O              329
#define FPGA_PIN_RAMA8_I              330
#define FPGA_PIN_RAMA9_T              316
#define FPGA_PIN_RAMA9_O              317
#define FPGA_PIN_RAMA9_I              318
#define FPGA_PIN_RAMA10_T             286
#define FPGA_PIN_RAMA10_O             287
#define FPGA_PIN_RAMA10_I             288
#define FPGA_PIN_RAMA11_T             304
#define FPGA_PIN_RAMA11_O             305
#define FPGA_PIN_RAMA11_I             306
#define FPGA_PIN_RAMA12_T             2
#define FPGA_PIN_RAMA12_O             3
#define FPGA_PIN_RAMA12_I             4
#define FPGA_PIN_RAMA13_T             340
#define FPGA_PIN_RAMA13_O             341
#define FPGA_PIN_RAMA13_I             342
#define FPGA_PIN_RAMA14_T             14
#define FPGA_PIN_RAMA14_O             15
#define FPGA_PIN_RAMA14_I             16
#define FPGA_PIN_RAMCE_T              271
#define FPGA_PIN_RAMCE_O              272
#define FPGA_PIN_RAMCE_I              273
#define FPGA_PIN_RAMOE_T              298
#define FPGA_PIN_RAMOE_O              299
#define FPGA_PIN_RAMOE_I              300
#define FPGA_PIN_RAMWR_T              5
#define FPGA_PIN_RAMWR_O              6
#define FPGA_PIN_RAMWR_I              7
#define FPGA_PIN_EPPDATA0_T           -1
#define FPGA_PIN_EPPDATA0_O           -1
#define FPGA_PIN_EPPDATA0_I           -1
#define FPGA_PIN_EPPDATA1_T           -1
#define FPGA_PIN_EPPDATA1_O           -1
#define FPGA_PIN_EPPDATA1_I           -1
#define FPGA_PIN_EPPDATA2_T           -1
#define FPGA_PIN_EPPDATA2_O           -1
#define FPGA_PIN_EPPDATA2_I           -1
#define FPGA_PIN_EPPDATA3_T           113
#define FPGA_PIN_EPPDATA3_O           114
#define FPGA_PIN_EPPDATA3_I           115
#define FPGA_PIN_EPPDATA4_T           122
#define FPGA_PIN_EPPDATA4_O           123
#define FPGA_PIN_EPPDATA4_I           124
#define FPGA_PIN_EPPDATA5_T           125
#define FPGA_PIN_EPPDATA5_O           126
#define FPGA_PIN_EPPDATA5_I           127
#define FPGA_PIN_EPPDATA6_T           128
#define FPGA_PIN_EPPDATA6_O           129
#define FPGA_PIN_EPPDATA6_I           130
#define FPGA_PIN_EPPDATA7_T           131
#define FPGA_PIN_EPPDATA7_O           132
#define FPGA_PIN_EPPDATA7_I           133
#define FPGA_PIN_EPPNADDRSTB_T        140
#define FPGA_PIN_EPPNADDRSTB_O        141
#define FPGA_PIN_EPPNADDRSTB_I        142
#define FPGA_PIN_EPPNDATASTB_T        86
#define FPGA_PIN_EPPNDATASTB_O        87
#define FPGA_PIN_EPPNDATASTB_I        88
#define FPGA_PIN_EPPNWRITE_T          89
#define FPGA_PIN_EPPNWRITE_O          90
#define FPGA_PIN_EPPNWRITE_I          91
#define FPGA_PIN_EPPNRESET_T          83
#define FPGA_PIN_EPPNRESET_O          84
#define FPGA_PIN_EPPNRESET_I          85
#define FPGA_PIN_EPPNWAIT_T           -1
#define FPGA_PIN_EPPNWAIT_O           -1
#define FPGA_PIN_EPPNWAIT_I           -1
#define FPGA_PIN_EPPNINTR_T           -1
#define FPGA_PIN_EPPNINTR_O           -1
#define FPGA_PIN_EPPNINTR_I           -1
#define FPGA_PIN_SPPSELECT_T          -1
#define FPGA_PIN_SPPSELECT_O          -1
#define FPGA_PIN_SPPSELECT_I          -1
#define FPGA_PIN_DAC0_T               47
#define FPGA_PIN_DAC0_O               48
#define FPGA_PIN_DAC0_I               49
#define FPGA_PIN_DAC1_T               56
#define FPGA_PIN_DAC1_O               57
#define FPGA_PIN_DAC1_I               58
#define FPGA_PIN_DAC2_T               59
#define FPGA_PIN_DAC2_O               60
#define FPGA_PIN_DAC2_I               61
#define FPGA_PIN_DAC3_T               68
#define FPGA_PIN_DAC3_O               69
#define FPGA_PIN_DAC3_I               70
#define FPGA_PIN_DAC4_T               71
#define FPGA_PIN_DAC4_O               72
#define FPGA_PIN_DAC4_I               73
#define FPGA_PIN_DAC5_T               41
#define FPGA_PIN_DAC5_O               42
#define FPGA_PIN_DAC5_I               43
#define FPGA_PIN_DAC6_T               38
#define FPGA_PIN_DAC6_O               39
#define FPGA_PIN_DAC6_I               40
#define FPGA_PIN_DINDAC_T             44
#define FPGA_PIN_DINDAC_O             45
#define FPGA_PIN_DINDAC_I             46
#define FPGA_PIN_DIN_T                80
#define FPGA_PIN_DIN_O                81
#define FPGA_PIN_DIN_I                82
#define FPGA_PIN_RXC_T                143
#define FPGA_PIN_RXC_O                144
#define FPGA_PIN_RXC_I                145
#define FPGA_PIN_RXD_T                164
#define FPGA_PIN_RXD_O                165
#define FPGA_PIN_RXD_I                166
#define FPGA_PIN_TXC_T                152
#define FPGA_PIN_TXC_O                153
#define FPGA_PIN_TXC_I                154
#define FPGA_PIN_TXD_T                178
#define FPGA_PIN_TXD_O                179
#define FPGA_PIN_TXD_I                180
#define FPGA_PIN_DCD_T                167
#define FPGA_PIN_DCD_O                168
#define FPGA_PIN_DCD_I                169
#define FPGA_PIN_MRES_T               199
#define FPGA_PIN_MRES_O               200
#define FPGA_PIN_MRES_I               201
#define FPGA_PIN_EPPNERR_T            202
#define FPGA_PIN_EPPNERR_O            203
#define FPGA_PIN_EPPNERR_I            204
#define FPGA_PIN_SPPPE_T              211
#define FPGA_PIN_SPPPE_O              212
#define FPGA_PIN_SPPPE_I              213
#define FPGA_PIN_CLKFPGA_T            175
#define FPGA_PIN_CLKFPGA_O            176
#define FPGA_PIN_CLKFPGA_I            177
#define FPGA_PIN_LEDPTT_T             190
#define FPGA_PIN_LEDPTT_O             191
#define FPGA_PIN_LEDPTT_I             192
#define FPGA_PIN_LEDDCD_T             17
#define FPGA_PIN_LEDDCD_O             18
#define FPGA_PIN_LEDDCD_I             19
#define FPGA_PIN_LEDSTA_T             26
#define FPGA_PIN_LEDSTA_O             27
#define FPGA_PIN_LEDSTA_I             28
#define FPGA_PIN_LEDCON_T             29
#define FPGA_PIN_LEDCON_O             30
#define FPGA_PIN_LEDCON_I             31
