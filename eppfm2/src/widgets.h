/* 
 * Custom Widgets
 * Copyright (C) 1999 Thomas Sailer <sailer@ife.ee.ethz.ch>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#ifndef __WIDGETS_H__
#define __WIDGETS_H__

#include <gdk/gdk.h>
#include <gtk/gtkwidget.h>

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

#define SCOPE(obj)          GTK_CHECK_CAST(obj, scope_get_type(), Scope)
#define SCOPE_CLASS(klass)  GTK_CHECK_CLASS_CAST(klass, scope_get_type(), ScopeClass)
#define IS_SCOPE(obj)       GTK_CHECK_TYPE(obj, scope_get_type())

typedef struct _Scope        Scope;
typedef struct _ScopeClass   ScopeClass;

struct _Scope
{
	GtkWidget widget;

	guint idlefunc;
	GdkGC *zeroline_gc;
	GdkGC *sampling_gc;
	GdkColor zerolinecol;
	GdkColor samplingcol;

	struct _ScopePoint *pt;
	guint ptnum, ptptr;
};

struct _ScopeClass
{
	GtkWidgetClass parent_class;
};


guint scope_get_type(void);
GtkWidget* scope_new(gchar *widget_name, gchar *str1, gchar *str2, gint int1, gint int2);
void scope_addvalue(Scope *trace, guint16 deltax, gint16 y, guint flags);
void scope_addvalues(Scope *trace, guint num, guint16 *deltax, gint16 *y);


#define SPECTRUM(obj)          GTK_CHECK_CAST(obj, spectrum_get_type(), Spectrum)
#define SPECTRUM_CLASS(klass)  GTK_CHECK_CLASS_CAST(klass, spectrum_get_type(), SpectrumClass)
#define IS_SPECTRUM(obj)       GTK_CHECK_TYPE(obj, spectrum_get_type())

typedef struct _Spectrum        Spectrum;
typedef struct _SpectrumClass   SpectrumClass;

struct _Spectrum
{
	GtkWidget widget;

	guint idlefunc;
	GdkGC *grid_gc;
	GdkGC *sampling_gc;
	GdkColor gridcol;
	GdkColor samplingcol;

	gint16 *pt;
	guint ptnum, ptptr;
};

struct _SpectrumClass
{
	GtkWidgetClass parent_class;
};


guint spectrum_get_type(void);
GtkWidget* spectrum_new(gchar *widget_name, gchar *str1, gchar *str2, gint int1, gint int2);
void spectrum_addvalue(Spectrum *trace, gint16 y);
void spectrum_addvalues(Spectrum *trace, guint num, gint16 *y);


#ifdef __cplusplus
}
#endif /* __cplusplus */


#endif /* __WIDGETS_H__ */
