#include <gtk/gtk.h>


void
on_file_activate                       (GtkMenuItem     *menuitem,
                                        gpointer         user_data);

void
on_dtmf_activate                       (GtkMenuItem     *menuitem,
                                        gpointer         user_data);

void
on_oscilloscope_activate               (GtkMenuItem     *menuitem,
                                        gpointer         user_data);

void
on_spectrum_activate                   (GtkMenuItem     *menuitem,
                                        gpointer         user_data);

void
on_quit_activate                       (GtkMenuItem     *menuitem,
                                        gpointer         user_data);

void
on_about_activate                      (GtkMenuItem     *menuitem,
                                        gpointer         user_data);

gboolean
on_dtmfwindow_key_press_event          (GtkWidget       *widget,
                                        GdkEventKey     *event,
                                        gpointer         user_data);

gboolean
on_dtmfwindow_key_release_event        (GtkWidget       *widget,
                                        GdkEventKey     *event,
                                        gpointer         user_data);

void
on_dtmf_key                            (GtkButton       *button,
                                        gpointer         user_data);

GtkWidget*
eppfm_scope_new (gchar *widget_name, gchar *string1, gchar *string2,
                gint int1, gint int2);

GtkWidget*
eppfm_spectrum_new (gchar *widget_name, gchar *string1, gchar *string2,
                gint int1, gint int2);

void
on_configok_clicked                    (GtkButton       *button,
                                        gpointer         user_data);

void
on_configquit_clicked                  (GtkButton       *button,
                                        gpointer         user_data);

gboolean
on_mainwindow_delete_event             (GtkWidget       *widget,
                                        GdkEvent        *event,
                                        gpointer         user_data);

void
on_mainwindow_destroy                  (GtkObject       *object,
                                        gpointer         user_data);

void
on_mainptt_toggled                     (GtkToggleButton *togglebutton,
                                        gpointer         user_data);

void
on_maindtmf_clicked                    (GtkButton       *button,
                                        gpointer         user_data);

void
on_mainscope_clicked                   (GtkButton       *button,
                                        gpointer         user_data);

void
on_mainspectrum_clicked                (GtkButton       *button,
                                        gpointer         user_data);

gboolean
on_mainwindow_key_press_event          (GtkWidget       *widget,
                                        GdkEventKey     *event,
                                        gpointer         user_data);

gboolean
on_mainwindow_key_release_event        (GtkWidget       *widget,
                                        GdkEventKey     *event,
                                        gpointer         user_data);

gboolean
on_aboutdialog_delete_event            (GtkWidget       *widget,
                                        GdkEvent        *event,
                                        gpointer         user_data);

gboolean
on_dtmfwindow_delete_event             (GtkWidget       *widget,
                                        GdkEvent        *event,
                                        gpointer         user_data);

gboolean
on_scopewindow_delete_event            (GtkWidget       *widget,
                                        GdkEvent        *event,
                                        gpointer         user_data);

gboolean
on_spectrumwindow_delete_event         (GtkWidget       *widget,
                                        GdkEvent        *event,
                                        gpointer         user_data);

void
on_aboutok_clicked                     (GtkButton       *button,
                                        gpointer         user_data);

GtkWidget*
scope_new (gchar *widget_name, gchar *string1, gchar *string2,
                gint int1, gint int2);

GtkWidget*
spectrum_new (gchar *widget_name, gchar *string1, gchar *string2,
                gint int1, gint int2);

void
on_dtmfclear_clicked                   (GtkButton       *button,
                                        gpointer         user_data);

gboolean
on_mainwindow_key_event                (GtkWidget       *widget,
                                        GdkEventKey     *event,
                                        gpointer         user_data);

gboolean
on_dtmfwindow_key_event                (GtkWidget       *widget,
                                        GdkEventKey     *event,
                                        gpointer         user_data);

gboolean
on_mainwindow_key_event                (GtkWidget       *widget,
                                        GdkEventKey     *event,
                                        gpointer         user_data);

void
on_button1750_clicked                  (GtkButton       *button,
                                        gpointer         user_data);

void
on_buttonsquelch_clicked               (GtkButton       *button,
                                        gpointer         user_data);

void
on_button1750_pressed                  (GtkButton       *button,
                                        gpointer         user_data);

void
on_button1750_released                 (GtkButton       *button,
                                        gpointer         user_data);
