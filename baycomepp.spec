# Note that this is NOT a relocatable package
%define ver      0.10
%define rel      1
%define prefix   /usr

Summary: Driver and diagnostic utilities for the Baycom EPP modem family
Name: baycomepp
Version: %ver
Release: %rel
Copyright: GPL
Group: Networking/Hamradio
Source: baycomepp-%{ver}.tar.gz
BuildRoot: /tmp/baycomepp-root
Packager: Thomas Sailer <sailer@ife.ee.ethz.ch>
Obsoletes: baycomfpga, eppconv
Requires: /sbin/ifconfig /sbin/route /sbin/arp

%description
This package contains the driver and diagnostic utilities
for the Baycom EPP modem family, specifically the
conventional EPP modem (using the IDT 7213x FIFO's) and
the EPPFLEX FPGA modem (using the Xilinx XCS10).

%prep
%setup

%build
mkdir obj
cd obj
../configure --prefix=%prefix --localstatedir=/var --enable-driver --enable-ppuser --enable-ppkdrv 
make

%install
rm -rf $RPM_BUILD_ROOT
cd obj
make prefix=$RPM_BUILD_ROOT%{prefix} install
install -g root -o root -m 0755 -d $RPM_BUILD_ROOT/var/run
install -g root -o root -m 0755 -d $RPM_BUILD_ROOT/etc/rc.d/init.d
install -g root -o root -m 0755 ../udriver/baycomepp.initscript $RPM_BUILD_ROOT/etc/rc.d/init.d/baycomepp

%clean
rm -rf $RPM_BUILD_ROOT

%files
%defattr(-, root, root)

%doc AUTHORS COPYING ChangeLog INSTALL NEWS README
%{prefix}/sbin/*
%{prefix}/bin/*
%{prefix}/man/*/*
/var/run
%{prefix}/libexec/*
%config /etc/rc.d/init.d/baycomepp
