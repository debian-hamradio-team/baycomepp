/*****************************************************************************/

/*
 *      drv.h  -- HDLC packet radio modem for EPP/ECP using FPGA usermode driver.
 *
 *      Copyright (C) 1998  Thomas Sailer (sailer@ife.ee.ethz.ch)
 *
 *      This program is free software; you can redistribute it and/or modify
 *      it under the terms of the GNU General Public License as published by
 *      the Free Software Foundation; either version 2 of the License, or
 *      (at your option) any later version.
 *
 *      This program is distributed in the hope that it will be useful,
 *      but WITHOUT ANY WARRANTY; without even the implied warranty of
 *      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *      GNU General Public License for more details.
 *
 *      You should have received a copy of the GNU General Public License
 *      along with this program; if not, write to the Free Software
 *      Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 *  Please note that the GPL allows you to use the driver, NOT the radio.
 *  In order to use the radio, you need a license from the communications
 *  authority of your country.
 *
 */

/*****************************************************************************/

#ifndef _DRV_H
#define _DRV_H

/* --------------------------------------------------------------------- */

#if defined(HAVE_SYS_IO_H)
#include <sys/io.h>
#elif defined(HAVE_ASM_IO_H)
#include <asm/io.h>
#endif

#include <syslog.h>
#include "fpga.h"

/* --------------------------------------------------------------------- */

#define KISS_FEND   ((unsigned char)0300)
#define KISS_FESC   ((unsigned char)0333)
#define KISS_TFEND  ((unsigned char)0334)
#define KISS_TFESC  ((unsigned char)0335)

#define KISS_CMD_DATA       0
#define KISS_CMD_TXDELAY    1
#define KISS_CMD_PPERSIST   2
#define KISS_CMD_SLOTTIME   3
#define KISS_CMD_TXTAIL     4
#define KISS_CMD_FULLDUP    5
#define KISS_CMD_HARDWARE   6
#define KISS_CMD_FECLEVEL   8
#define KISS_CMD_RETURN     255

/* --------------------------------------------------------------------- */

#define MAXFLEN           512
#define RXBUFFER_SIZE     ((MAXFLEN*6/5)+8)
#define TXBUFFER_SIZE     8192

#define KISSOUTBUF_SIZE   8192
#define KISSINBUF_SIZE    (2*MAXFLEN+8)

struct baycomepp_state {
	int kissfd;

	struct adapter_config config;

	struct {
		unsigned bitrate, bitrateby25;
		int slotcnt, flagcnt;
		int dcd, ptt;
		int calib;
		enum { tx_idle = 0, tx_keyup, tx_data, tx_tail } state;
	} access;

        struct chaccesspar {
                unsigned int txdelay;
                unsigned int slottime;
                unsigned int ppersist;
                unsigned int txtail;
                unsigned int fulldup;
        } chaccesspar;

	struct hdlc_receiver {
		unsigned int bitbuf, bitstream, numbits, state;
		unsigned char *bufptr;
		int bufcnt;
		unsigned char buf[RXBUFFER_SIZE];
	} hrx;

	struct hdlc_transmitter {
		unsigned rd, wr;
		u_int8_t buf[TXBUFFER_SIZE];
	} htx;

        struct {
                unsigned ptr;
                u_int8_t b[KISSINBUF_SIZE];
        } kissin;

        struct {
                unsigned rd;
                unsigned wr;
                u_int8_t b[KISSOUTBUF_SIZE];
        } kissout;

        struct statistics {
		unsigned long pkt_in;
		unsigned long pkt_out;
		unsigned long pkt_outerr;
		unsigned long bytes_in;
		unsigned long bytes_out;
                unsigned long kiss_out;
                unsigned long kiss_outerr;
                unsigned long kiss_in;
                unsigned long kiss_inerr;
                unsigned long ptt_keyed;
		unsigned int intcnt, icnt1, icnt2, icnt3, ocnt1, ocnt2, ocnt3;
		u_int8_t status;
        } stat;

};

/* --------------------------------------------------------------------- */

extern int logging;
extern int logpackets;
extern struct baycomepp_state state;
extern const char *progname;

/* --------------------------------------------------------------------- */

#define SEV_FATAL    LOG_CRIT
#define SEV_ERROR    LOG_ERR
#define SEV_WARNING  LOG_WARNING
#define SEV_NOTICE   LOG_NOTICE
#define SEV_INFO     LOG_INFO
#define SEV_DEBUG    LOG_DEBUG

extern void errprintf(int severity, const char *fmt, ...) __attribute__ ((format (printf, 2, 3)));
extern void errstr(int severity, const char *st);
extern void sighandler(int sig);
extern void sig_usr1(int sig);
extern void sig_usr2(int sig);

/* ---------------------------------------------------------------------- */

#ifdef __i386__
#define GETTICK(x)                                                \
({                                                                \
	__asm__ __volatile__("rdtsc" : "=a" (x) : : "dx");        \
})
#else /* __i386__ */
#define GETTICK(x)  ((x)=0)
#endif /* __i386__ */

/* --------------------------------------------------------------------- */

#define MODE_EPP     0
#define MODE_ECP     1
#define MODE_EPPAFSK 2
#define MODE_EPPCONV 3  /* conventional modem */

/* --------------------------------------------------------------------- */
#endif /* _DRV_H */
