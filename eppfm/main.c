/*****************************************************************************/

/*
 *      main.c  --  Main routines for "virtual transceiver".
 *
 *      Copyright (C) 1999-2000  Thomas Sailer (sailer@ife.ee.ethz.ch)
 *
 *      This program is free software; you can redistribute it and/or modify
 *      it under the terms of the GNU General Public License as published by
 *      the Free Software Foundation; either version 2 of the License, or
 *      (at your option) any later version.
 *
 *      This program is distributed in the hope that it will be useful,
 *      but WITHOUT ANY WARRANTY; without even the implied warranty of
 *      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *      GNU General Public License for more details.
 *
 *      You should have received a copy of the GNU General Public License
 *      along with this program; if not, write to the Free Software
 *      Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 *  Please note that the GPL allows you to use the driver, NOT the radio.
 *  In order to use the radio, you need a license from the communications
 *  authority of your country.
 *
 */

/*****************************************************************************/

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#define G_LOG_DOMAIN "eppfm"

#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#ifdef HAVE_SYSLOG_H
#include <syslog.h>
#endif

#ifdef HAVE_GETOPT_H
#include <getopt.h>
#endif

#include <glib.h>
#include <gtk/gtk.h>

#include "eppfm.h"

#include "interface.h"
#include "support.h"

/* ---------------------------------------------------------------------- */

static struct adapter_config cfg = { 19666600, 9600, 0, 0, 0, 1, 1, 0, 1 };

static unsigned verboselevel = 0;
static unsigned syslogmsg = 0;

/* ---------------------------------------------------------------------- */

/*OutputDebugStringA*/
int lprintf(unsigned vl, const char *format, ...)
{
        va_list ap;
        int r;

        if (vl > verboselevel)
                return 0;
        va_start(ap, format);
#ifdef HAVE_VSYSLOG
        if (syslogmsg) {
                static const int logprio[] = { LOG_ERR, LOG_INFO };
                vsyslog((vl > 1) ? LOG_DEBUG : logprio[vl], format, ap);
                r = 0;
        } else
#endif
                r = vfprintf(stderr, format, ap);
        va_end(ap);
        return r;
}

/* ---------------------------------------------------------------------- */

static gint periodic(gpointer data)
{
	GtkWidget *mainwindow = (GtkWidget *)data;
	GtkWidget *meterrx;
	GtkWidget *metertx;
	unsigned ilvl, olvl;

	ilvl = audio_getilevel();
	olvl = audio_getolevel();
	printf("ilvl %5u  log %6.2f\n", ilvl, log(ilvl ? ilvl : 1) * (1.0 / 11.09));
	meterrx = GTK_WIDGET(gtk_object_get_data(GTK_OBJECT(mainwindow), "meterrx"));
	metertx = GTK_WIDGET(gtk_object_get_data(GTK_OBJECT(mainwindow), "metertx"));
	gtk_progress_set_percentage(GTK_PROGRESS(meterrx), log(ilvl ? ilvl : 1) * (1.0 / 11.09));
	gtk_progress_set_percentage(GTK_PROGRESS(metertx), log(olvl ? olvl : 1) * (1.0 / 11.09));
	return TRUE;
}

/* ---------------------------------------------------------------------- */

int main(int argc, char *argv[])
{
	GtkWidget *mainwindow;
	gint tmtag;
        int c, err = 0;
        unsigned iobase = 0x378;

        printf("EPP virtual transceiver  (c) 1998-2000 by Thomas Sailer, HB9JNX/AE4WA\n");
#if 0
	bindtextdomain (PACKAGE, PACKAGE_LOCALE_DIR);
	textdomain (PACKAGE);
#endif

	gtk_set_locale();
	gtk_init(&argc, &argv);

#if 0
	add_pixmap_directory(PACKAGE_DATA_DIR "/pixmaps");
	add_pixmap_directory(PACKAGE_SOURCE_DIR "/pixmaps");
#endif

        while ((c = getopt(argc, argv, "svp:m:")) != EOF) {
                switch (c) {
                case 'p': 
                        iobase = strtoul(optarg, NULL, 0);
                        if (iobase <= 0 || iobase >= 0x3f8)
                                err++;
                        break;

		case 'm':
			parseconfig(&cfg, optarg);
			break;

                case 'v':
                        verboselevel++;
                        break;

#if defined(HAVE_SYSLOG) && defined(HAVE_VSYSLOG)
                case 's':
                        if (syslogmsg)
                                break;
                        openlog("eppfpga", LOG_PID, LOG_USER);
                        syslogmsg = 1;
                        break;
#endif

                default:
                        err++;
                        break;
                }
        }
        if (err) {
                g_message("usage: %s [-v] [-p <portaddr>] [-m <modestr>]\n", argv[0]);
                exit(1);
        }
        if (parport_init_direct(iobase)) {
                g_error("no parport found at 0x%x\n", iobase);
                exit(1);
        }
	if (audio_start(&cfg)) {
		g_error("Cannot start audio services\n");
                exit(1);
	}
	mainwindow = create_mainwindow();
	gtk_widget_show(mainwindow);
	tmtag = gtk_timeout_add(250, periodic, mainwindow);
	gtk_main();
	gtk_timeout_remove(tmtag);
	audio_stop();
	return 0;
}

