#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <stdio.h>
#include <string.h>
#include "eppfm.h"
#include <gtk/gtk.h>

#include "callbacks.h"
#include "interface.h"
#include "support.h"

void on_button1750_pressed(GtkButton *button, gpointer user_data)
{
	audio_1750(1);
	audio_ptt(1);
}

void on_button1750_released(GtkButton *button, gpointer user_data)
{
	audio_1750(0);
	audio_ptt(0);
}

void on_buttonptt_pressed(GtkButton *button, gpointer user_data)
{
	audio_ptt(1);
}

void on_buttonptt_released(GtkButton *button, gpointer user_data)
{
	audio_ptt(0);
}

void on_buttondtmf(GtkButton *button, gpointer user_data)
{
	unsigned int data = (unsigned int)user_data;

	if (data >= 0x10)
		audio_dtmf(data & 15);
	else
		audio_dtmf(-1);
}

void on_exit_activate(GtkMenuItem *menuitem, gpointer user_data)
{
	gtk_main_quit();
}

void on_about_activate(GtkMenuItem *menuitem, gpointer user_data)
{
	printf("blah\n");
}

gboolean on_mainwindow_delete_event(GtkWidget *widget, GdkEvent *event, gpointer user_data)
{
	return FALSE;
}

void on_mainwindow_destroy(GtkObject *object, gpointer user_data)
{
	gtk_main_quit();
}

gboolean on_mainwindow_key_press_event(GtkWidget *widget, GdkEventKey *event, gpointer user_data)
{
	const char dtmfch[] = "0123456789ABCD*#";
	char *cp;
	unsigned v;

	cp = strchr(dtmfch, event->keyval);
	if (!cp)
		return FALSE;
	v = cp - dtmfch;
	if (v >= 16)
		return FALSE;
	audio_dtmf(v);
	return TRUE;
	/*printf("key_press: keyval %u length %d\n", event->keyval, event->length);*/
}

gboolean on_mainwindow_key_release_event(GtkWidget *widget, GdkEventKey *event, gpointer user_data)
{
	const char dtmfch[] = "0123456789ABCD*#";
	char *cp;
	unsigned v;

	cp = strchr(dtmfch, event->keyval);
	if (!cp)
		return FALSE;
	v = cp - dtmfch;
	if (v >= 16)
		return FALSE;
	audio_dtmf(-1);
	return TRUE;
	/*printf("key_release: keyval %u length %d\n", event->keyval, event->length);*/
}
