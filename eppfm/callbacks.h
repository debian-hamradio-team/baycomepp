#include <gtk/gtk.h>


gboolean
on_mainwindow_key_press_event          (GtkWidget       *widget,
                                        GdkEventKey     *event,
                                        gpointer         user_data);

gboolean
on_mainwindow_key_release_event        (GtkWidget       *widget,
                                        GdkEventKey     *event,
                                        gpointer         user_data);

gboolean
on_mainwindow_delete_event             (GtkWidget       *widget,
                                        GdkEvent        *event,
                                        gpointer         user_data);

void
on_mainwindow_destroy                  (GtkObject       *object,
                                        gpointer         user_data);

void
on_exit_activate                       (GtkMenuItem     *menuitem,
                                        gpointer         user_data);

void
on_about_activate                      (GtkMenuItem     *menuitem,
                                        gpointer         user_data);

void
on_button1750_pressed                  (GtkButton       *button,
                                        gpointer         user_data);

void
on_button1750_released                 (GtkButton       *button,
                                        gpointer         user_data);

void
on_buttonptt_pressed                   (GtkButton       *button,
                                        gpointer         user_data);

void
on_buttonptt_released                  (GtkButton       *button,
                                        gpointer         user_data);

void
on_buttondtmf                          (GtkButton       *button,
                                        gpointer         user_data);

void
on_buttondtmf                          (GtkButton       *button,
                                        gpointer         user_data);
