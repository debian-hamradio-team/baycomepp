/*****************************************************************************/

/*
 *      eppfm.h  --  Function prototypes for "virtual transceiver".
 *
 *      Copyright (C) 1999  Thomas Sailer (sailer@ife.ee.ethz.ch)
 *
 *      This program is free software; you can redistribute it and/or modify
 *      it under the terms of the GNU General Public License as published by
 *      the Free Software Foundation; either version 2 of the License, or
 *      (at your option) any later version.
 *
 *      This program is distributed in the hope that it will be useful,
 *      but WITHOUT ANY WARRANTY; without even the implied warranty of
 *      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *      GNU General Public License for more details.
 *
 *      You should have received a copy of the GNU General Public License
 *      along with this program; if not, write to the Free Software
 *      Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 *  Please note that the GPL allows you to use the driver, NOT the radio.
 *  In order to use the radio, you need a license from the communications
 *  authority of your country.
 *
 */

/*****************************************************************************/

#ifndef _EPPFM_H
#define _EPPFM_H

/* ---------------------------------------------------------------------- */

#include "fpga.h"

/* ---------------------------------------------------------------------- */

extern const int16_t costab[512];
#define COS(x) costab[((x)>>7)&0x1ff]
#define SIN(x) COS((x)+0xc000)

/* ---------------------------------------------------------------------- */

extern void audio_ptt(int ptt);
extern void audio_stop(void);
extern int audio_start(struct adapter_config *cfg);
extern void audio_1750(int on);
extern void audio_dtmf(int ch);
extern unsigned audio_getilevel(void);
extern unsigned audio_getolevel(void);

/* ---------------------------------------------------------------------- */
#endif /* _EPPFM_H */
